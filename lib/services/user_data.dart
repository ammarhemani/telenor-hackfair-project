import 'package:shared_preferences/shared_preferences.dart';

class User {
  static String nickname;
  static List challenges;
  static bool firstLaunch;

  static Future<void> setInitialUserPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    nickname = prefs.getString('nicknamePref') ?? '';
    if (prefs.getBool('firstLaunchPref') == null) {
      firstLaunch = true;
      prefs.setBool('firstLaunchPref', false);
    } else {
      firstLaunch = false;
    }
  }
}
