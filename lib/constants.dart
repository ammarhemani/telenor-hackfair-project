import 'package:flutter/material.dart';

//Color kPrimaryColor = Color(0xFF0E7D85);
Color kPrimaryColor = Color(0xFF1F344B);
Color kSecondaryColor = Color(0xFF152A3F);

Color kButtonColor = Color(0xFFFC5B19);

Color kContainerColor1 = Color(0xFF6C75E1);
Color kContainerColor2 = Color(0xFFFC5B19);
Color kContainerColor3 = Color(0xCCFFC331);

List<String> kChallenges = [
  'Anxiety',
  'Motivation',
  'Confidence',
  'Sleep',
  'Depression',
  'Work Stress',
  'Relationships',
  'Exam Stress',
  'Pregnancy',
  'Loss',
  'LGBTQ+',
  'Low energy',
  'Self esteem',
  'Loneliness',
  'Trauma',
  'Health Issues',
];
