import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:telenor_hackfair_app/components/chat_message.dart';
import 'package:telenor_hackfair_app/components/fade_in.dart';
import 'package:telenor_hackfair_app/components/quick_reply.dart';
import 'package:telenor_hackfair_app/constants.dart';

import '../components/fade_in.dart';
import 'package:telenor_hackfair_app/services/user_data.dart';

class ChatScreen extends StatefulWidget {
  static String id = 'chat_screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final List<ChatMessage> _messages = <ChatMessage>[];
  final List<QuickReply> _quickReplies = <QuickReply>[];
  final TextEditingController _textController = TextEditingController();
  final _messagesListKey = GlobalKey<AnimatedListState>();
  final _quickRepliesListKey = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    super.initState();
    _response('hello, my name is ${User.nickname}');
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [kSecondaryColor, kPrimaryColor],
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
//                Text(
//                  'Bud',
//                ),
                Image.asset(
                  'lib/assets/avatar.gif',
                  height: 50,
                ),
              ],
            ),
            centerTitle: false,
          ),
          body: SafeArea(
            child: Column(children: <Widget>[
              Flexible(
                flex: 10,
                child: AnimatedList(
                    key: _messagesListKey,
                    initialItemCount: 0,
                    padding: EdgeInsets.all(8.0),
                    reverse: true,
                    itemBuilder: (_, index, animation) {
                      return FadeTransition(
                        opacity: animation,
                        child: _messages[index],
                      );
                    }),
              ),
              Flexible(
                flex: 1,
                child: AnimatedList(
                    key: _quickRepliesListKey,
                    scrollDirection: Axis.horizontal,
                    initialItemCount: 0,
                    padding: EdgeInsets.all(8.0),
                    reverse: true,
                    shrinkWrap: true,
                    itemBuilder: (_, index, animation) {
                      return FadeTransition(
                        opacity: animation,
                        child: GestureDetector(
                          onTap: () {
                            _handleSubmitted(_quickReplies[index].text);
                            _clearAllQuickReplies();
                          },
                          child: _quickReplies[index],
                        ),
                      );
                    }),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.0),
                child: Row(
                  children: <Widget>[
                    Flexible(
                      child: FadeIn(
                        5.0,
                        Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).cardColor,
                            borderRadius: BorderRadius.circular(30),
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: TextField(
                            style: TextStyle(color: kPrimaryColor),
                            controller: _textController,
                            onSubmitted: _handleSubmitted,
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20, 12, 12, 12),
                              border: InputBorder.none,
                              suffixIcon: CircleAvatar(
                                backgroundColor: kButtonColor,
                                child: IconButton(
                                  iconSize: 20.0,
                                  icon: Icon(
                                    Icons.send,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    _handleSubmitted(_textController.text);
                                  },
                                ),
                              ),
                              hintText: "Reply or say 'Help'",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ],
    );
  }

  void _clearAllQuickReplies() {
    for (var i = 0; i <= _quickReplies.length - 1; i++) {
      _quickRepliesListKey.currentState.removeItem(0,
          (BuildContext context, Animation<double> animation) {
        return Container();
      });
    }
    _quickReplies.clear();
  }

  void _handleSubmitted(String text) {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _textController.clear());
    ChatMessage message = ChatMessage(
      text: text,
      user: true,
    );
    setState(() {
      _messages.insert(0, message);
      _messagesListKey.currentState.insertItem(0);
    });
    _response(text);
  }

  void _response(query) async {
    //DialogFlow Authentication
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _textController.clear());
    AuthGoogle authGoogle =
        await AuthGoogle(fileJson: "lib/assets/dialogflow-auth.json").build();
    Dialogflow dialogflow =
        Dialogflow(authGoogle: authGoogle, language: Language.english);
    AIResponse response = await dialogflow.detectIntent(query);

    //logic for inserting the right type of message/quick reply in the animated list
    for (Map messageMap in response.getListMessage()) {
      ChatMessage message;
      QuickReply quickReply;

      if (messageMap.containsKey('payload')) {
        int type = messageMap['payload']['type'];
        if (type == 0) {
          for (String text in messageMap['payload']['quickReplies']) {
            quickReply = QuickReply(
              text: text,
            );
            _quickReplies.insert(0, quickReply);
            _quickRepliesListKey.currentState.insertItem(0);
          }
          print(_quickReplies[0].text);
          print(_quickReplies[1].text);
        } else if (type == 2) {
          message = ChatMessage(
            type: type,
            imageUri: messageMap['payload']['imageUri'],
            user: false,
          );
        }
      } else {
        message = ChatMessage(
          text: messageMap['text']['text'][0],
          type: 1,
          user: false,
        );
      }

      setState(() {
        _messages.insert(0, message);
        _messagesListKey.currentState.insertItem(0);
      });
    }
  }
}
