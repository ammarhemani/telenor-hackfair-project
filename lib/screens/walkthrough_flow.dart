import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:telenor_hackfair_app/components/fade_in.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:flutter/services.dart';
import 'package:telenor_hackfair_app/screens/home_screen.dart';
import 'package:telenor_hackfair_app/services/user_data.dart';

class WalkthroughFlow extends StatefulWidget {
  static String id = 'walkthrough_flow';
  @override
  _WalkthroughFlowState createState() => _WalkthroughFlowState();
}

class _WalkthroughFlowState extends State<WalkthroughFlow> {
  PageController _pageController = PageController();

  bool anxietyChecked = false;
  bool lossChecked = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(30.0),
          child: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Center(
                        child: FadeIn(
                          3.0,
                          Text(
                            'Hey! I\'m Bud',
                            style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      FadeIn(
                        5.0,
                        Text(
                          'Our conversations will be private & anonymous, so there is no login. Just choose a nickname and we\'re good to go.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            height: 1.5,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  FadeIn(
                    7.0,
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.grey[200],
                      ),
                      child: TextFormField(
                        style: TextStyle(color: kPrimaryColor),
                        onChanged: (newValue) {
                          User.nickname = newValue;
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Required';
                          }
                          return null;
                        },
                        maxLines: null,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            onPressed: () async {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              prefs.setString('nicknamePref', User.nickname);

                              FocusScope.of(context).requestFocus(FocusNode());
                              _pageController.animateToPage(1,
                                  duration: Duration(seconds: 1),
                                  curve: Curves.fastLinearToSlowEaseIn);
                            },
                            icon: CircleAvatar(
                              backgroundColor: kButtonColor,
                              foregroundColor: Colors.white,
                              child: Icon(
                                Icons.arrow_upward,
                              ),
                            ),
                          ),
//                          labelText: 'Nickname',
                          hintText: 'Choose a nickname...',
                          isDense: false,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                        ),
                      ),
                    ),
                  ),
                  FadeIn(
                    9.0,
                    Container(
                      width: 250,
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle(fontSize: 15.0, color: Colors.white),
                          children: [
                            TextSpan(text: 'By continuing you agree to our '),
                            TextSpan(
                              text: 'Terms of Service',
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                            TextSpan(text: ' and '),
                            TextSpan(
                              text: 'Privacy Policy',
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Center(
                        child: Container(
                          width: 300,
                          child: FadeIn(
                            3.0,
                            Text(
                              'Welcome to ${User.nickname}\'s space',
                              style: TextStyle(
                                fontSize: 35,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      FadeIn(
                        5.0,
                        Text(
                          'Here you can breathe, meditate or talk things through. Over time, this space will personalise for your unique challenges and needs.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            height: 1.5,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  FadeIn(
                    7.0,
                    Image.asset('lib/assets/bud-avatar.gif'),
                  ),
                  FadeIn(
                    9.0,
                    RaisedButton(
                      onPressed: () {
                        _pageController.animateToPage(2,
                            duration: Duration(seconds: 1),
                            curve: Curves.fastLinearToSlowEaseIn);
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      color: kButtonColor,
                      padding:
                          EdgeInsets.symmetric(horizontal: 35, vertical: 10),
                      child: Text(
                        'Build my space',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(top: 70.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                'Building your space...',
                                style: TextStyle(
                                  fontSize: 35,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 30),
                              Text(
                                'Select challenges that you would like bud to help you with.',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 20,
                                  height: 1.5,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  anxietyChecked
                                      ? _checkedAnxietyButton()
                                      : Expanded(
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 50, vertical: 30),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              color: Colors.orange[400]
                                                  .withOpacity(.8),
                                            ),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  anxietyChecked = true;
                                                });
                                              },
                                              child: Text(
                                                'Anxiety',
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.blueGrey[200]
                                            .withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Motivation',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.red[200].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Confidence',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.blue[200].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Sleep',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color:
                                            Colors.green[200].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Depression',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.red[400].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Work Stress',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color:
                                            Colors.orange[200].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Relationships',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.grey[600].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Exam Stress',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.greenAccent[200]
                                            .withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Pregnancy',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  lossChecked
                                      ? _checkedLossButton()
                                      : Expanded(
                                          child: Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 30, vertical: 30),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              color: Colors.purple[200]
                                                  .withOpacity(.8),
                                            ),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  lossChecked = true;
                                                });
                                              },
                                              child: Text(
                                                'Loss',
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                              SizedBox(height: 20),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 50, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color:
                                            Colors.orange[200].withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Anxiety',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 30, vertical: 30),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.blueGrey[200]
                                            .withOpacity(.8),
                                      ),
                                      child: Text(
                                        'Motivation',
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 120),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.pushReplacementNamed(
                              context, HomeScreen.id);
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        color: kButtonColor,
                        padding:
                            EdgeInsets.symmetric(horizontal: 35, vertical: 20),
                        child: Text(
                          'Build my space',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _checkedAnxietyButton() {
    return Expanded(
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.orange[400].withOpacity(.8),
            ),
            child: GestureDetector(
              onTap: () {},
              child: Text(
                'Anxiety',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.orange.withOpacity(.6),
            ),
            child: Center(
              child: CircleAvatar(
                backgroundColor: kButtonColor,
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _checkedLossButton() {
    return Expanded(
      child: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.purple[200].withOpacity(.8),
            ),
            child: GestureDetector(
              onTap: () {},
              child: Text(
                'Loss',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.purple[200].withOpacity(.6),
            ),
            child: Center(
              child: CircleAvatar(
                backgroundColor: kButtonColor,
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
