import 'package:flutter/material.dart';
import 'package:telenor_hackfair_app/constants.dart';

class SettingsScreen extends StatefulWidget {
  static String id = 'settings_screen';
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        backgroundColor: kPrimaryColor,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.settings,
                      color: kButtonColor,
                    ),
                    title: Text('Settings'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.lock,
                      color: kButtonColor,
                    ),
                    title: Text('Set a lock'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.notifications_none,
                      color: kButtonColor,
                    ),
                    title: Text('Notifications'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                SizedBox(height: 30),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.share,
                      color: kButtonColor,
                    ),
                    title: Text('Share'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                SizedBox(height: 30),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.feedback,
                      color: kButtonColor,
                    ),
                    title: Text('Feedback'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.chat_bubble_outline,
                      color: kButtonColor,
                    ),
                    title: Text('FAQ'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.lock,
                      color: kButtonColor,
                    ),
                    title: Text('Privacy Policy'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
                Card(
                  color: kPrimaryColor,
                  margin: EdgeInsets.all(0),
                  child: ListTile(
                    leading: Icon(
                      Icons.find_in_page,
                      color: kButtonColor,
                    ),
                    title: Text('Terms & Conditions'),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ),
              ],
            ),
            Text(
              'Version 1.0.0',
              style: Theme.of(context).textTheme.caption,
            ),
          ],
        ),
      ),
    );
  }
}
