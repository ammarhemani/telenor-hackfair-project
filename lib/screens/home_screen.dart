import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:telenor_hackfair_app/components/home_tabs/home_tab.dart';
import 'package:telenor_hackfair_app/components/home_tabs/therapist_tab.dart';
import 'package:telenor_hackfair_app/screens/settings_screen.dart';
import 'package:telenor_hackfair_app/services/user_data.dart';
import 'package:telenor_hackfair_app/components/home_tabs/journal_tab.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  static String id = 'home_screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedIndex = 0;

  PageController controller = PageController();

  _launchCaller() async {
    const url = "tel:1234567";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchCallPopup() async {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text('Call Local Helpline?'),
            content: Text('You are not alone, help is just a call away'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('Cancel'),
                isDestructiveAction: true,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              CupertinoDialogAction(
                child: Text('Call'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  List<GButton> tabs = [
    GButton(
      gap: 30,
      iconActiveColor: kContainerColor1,
      iconColor: kPrimaryColor,
      textColor: kContainerColor1,
      color: kContainerColor1.withOpacity(.2),
      iconSize: 24,
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
      icon: Icons.home,
      // textStyle: t.textStyle,
      text: 'Home',
    ),
    GButton(
      gap: 30,
      iconActiveColor: kContainerColor2,
      iconColor: kPrimaryColor,
      textColor: kContainerColor2,
      color: kContainerColor2.withOpacity(.2),
      iconSize: 24,
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
      icon: Icons.question_answer,
      // textStyle: t.textStyle,
      text: 'Therapist',
    ),
    GButton(
      gap: 30,
      iconActiveColor: kContainerColor3,
      iconColor: kPrimaryColor,
      textColor: kContainerColor3,
      color: kContainerColor3.withOpacity(.2),
      iconSize: 24,
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 5),
      icon: Icons.note,
      // textStyle: t.textStyle,
      text: 'Journal',
    ),
  ];

  Text _handleAppBarTitle() {
    if (selectedIndex == 0) {
      return Text('${User.nickname}\'s space');
    } else if (selectedIndex == 1) {
      return Text('Get professional help');
    } else if (selectedIndex == 2) {
      return Text('My Journal');
    }
    return Text('You\'re loved');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: _handleAppBarTitle(),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: GestureDetector(
              onTap: () {
//                _launchCaller();
                _launchCallPopup();
              },
              child: Text(
                'SOS',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushNamed(context, SettingsScreen.id);
            },
          ),
        ],
      ),
      body: PageView(
        onPageChanged: (page) {
          setState(() {
            selectedIndex = page;
          });
        },
        controller: controller,
        children: <Widget>[
          HomeTab(),
          TherapistTab(),
          JournalTab(),
        ],
      ),
      bottomNavigationBar: SafeArea(
        child: Container(
          margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100)),
              boxShadow: [
                BoxShadow(
                    spreadRadius: -10,
                    blurRadius: 60,
                    color: Colors.black.withOpacity(.20),
                    offset: Offset(0, 15))
              ]),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5),
            child: GNav(
                tabs: tabs,
                selectedIndex: selectedIndex,
                onTabChange: (index) {
                  print(index);
                  setState(() {
                    selectedIndex = index;
                  });
                  controller.jumpToPage(index);
                }),
          ),
        ),
      ),
    );
  }
}
