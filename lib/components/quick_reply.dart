import 'package:flutter/material.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:telenor_hackfair_app/screens/chat_screen.dart';

class QuickReply extends StatelessWidget {
  QuickReply({this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: kContainerColor3,
        borderRadius: BorderRadius.circular(30),
      ),
      child: Text(text, style: TextStyle(fontSize: 18)),
    );
  }
}
