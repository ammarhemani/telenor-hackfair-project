import 'package:flutter/material.dart';
import 'package:telenor_hackfair_app/components/pricing_container.dart';

class TherapistTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 25.0),
        child: Column(
          children: <Widget>[
            Text(
              'Get a Therapist',
              style: Theme.of(context).textTheme.headline,
            ),
            SizedBox(height: 20),
            Text(
              '\"This app has helped me through some incredibly tough times, my life is much better than it was a year ago.\" - Ammar',
              style: Theme.of(context).textTheme.subhead,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 50),
            PricingContainer(
              title: 'Weekly',
            ),
            SizedBox(height: 20),
            PricingContainer(
              title: 'Monthly',
            ),
            SizedBox(height: 20),
            PricingContainer(
              title: 'Quarterly',
            ),
          ],
        ),
      ),
    );
  }
}
