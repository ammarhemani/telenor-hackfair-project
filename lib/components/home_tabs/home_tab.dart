import 'package:flutter/material.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:telenor_hackfair_app/screens/chat_screen.dart';

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 25),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: kContainerColor1,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'MY 4 AM FRIEND',
                        style: Theme.of(context).textTheme.body2,
                      ),
                      SizedBox(height: 30),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Image.asset(
                            'lib/assets/avatar.gif',
                            height: 100,
                            width: 100,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 210,
                                child: Text(
                                  'How are you feeling?',
                                  style: Theme.of(context).textTheme.headline,
                                ),
                              ),
                              SizedBox(height: 30),
                              RaisedButton(
                                onPressed: () {
                                  Navigator.pushNamed(context, ChatScreen.id);
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                color: Colors.white.withOpacity(.3),
                                elevation: 0.0,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 35, vertical: 10),
                                child: Text(
                                  'Talk now',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: kContainerColor2,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'HIRE A THERAPIST',
                        style: Theme.of(context).textTheme.body2,
                      ),
                      SizedBox(height: 30),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                  color: Colors.white,
                                )),
                            child: CircleAvatar(
                              radius: 40,
                              child: Icon(Icons.lock),
                              backgroundColor: Colors.white.withOpacity(.2),
                              foregroundColor: Colors.white,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 200,
                                child: Text(
                                  'No upcoming sessions',
                                  style: Theme.of(context).textTheme.headline,
                                ),
                              ),
                              SizedBox(height: 30),
                              RaisedButton(
                                onPressed: () {},
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                color: Colors.white.withOpacity(.3),
                                elevation: 0.0,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 35, vertical: 10),
                                child: Text(
                                  'Book a session',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: kContainerColor3,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'WRITE IT OUT ',
                        style: Theme.of(context).textTheme.body2,
                      ),
                      SizedBox(height: 30),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                                border: Border.all(
                                  color: kContainerColor3,
                                )),
                            child: CircleAvatar(
                              radius: 40,
                              child: Icon(Icons.wrap_text),
                              backgroundColor: Colors.white.withOpacity(.2),
                              foregroundColor: Colors.white,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width: 200,
                                child: Text(
                                  'Writing out your fears can help',
                                  style: Theme.of(context).textTheme.headline,
                                ),
                              ),
                              SizedBox(height: 30),
                              RaisedButton(
                                onPressed: () {},
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                color: Colors.white.withOpacity(.3),
                                elevation: 0.0,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 35, vertical: 10),
                                child: Text(
                                  'Make an entry',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
