import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bubble/bubble.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:transparent_image/transparent_image.dart';

class ChatMessage extends StatelessWidget {
  ChatMessage({this.text, this.imageUri, this.type, this.user});

  final String text;
  final String imageUri;
  final int type;
  final bool user;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: this.user ? myMessage(context) : botMessage(context),
      ),
    );
  }

  List<Widget> botMessage(context) {
    /*
    Message types:
    1: Text Message
    2: Image
    */

    switch (type) {
      case 1:
        return <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Bubble(
                  margin: BubbleEdges.only(right: 75),
                  padding: BubbleEdges.all(10),
                  alignment: Alignment.topLeft,
                  nip: BubbleNip.leftTop,
                  color: kContainerColor1,
                  child: Text(
                    text,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
        ];
      case 2:
        return <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Bubble(
                  margin: BubbleEdges.only(right: 75),
                  padding: BubbleEdges.all(10),
                  alignment: Alignment.topLeft,
                  nip: BubbleNip.leftTop,
                  color: kContainerColor1,
                  child: Container(
                    constraints: BoxConstraints(
                      minWidth: 200,
                      minHeight: 200,
                    ),
                    child: FadeInImage.memoryNetwork(
                      image: imageUri ??
                          'https://i.pinimg.com/originals/b8/b8/f7/b8b8f787c454cf1ded2d9d870707ed96.png',
                      placeholder: kTransparentImage,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ];
      default:
        return <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Bubble(
                  margin: BubbleEdges.only(right: 75),
                  padding: BubbleEdges.all(10),
                  alignment: Alignment.topLeft,
                  nip: BubbleNip.leftTop,
                  child: Container(
                      constraints: BoxConstraints(
                        minWidth: 200,
                        minHeight: 200,
                      ),
                      child: Text(
                          'Oops, something went wrong.. try again please')),
                ),
              ],
            ),
          ),
        ];
    }
  }

//  List<Widget> otherMessage(context) {
//    return <Widget>[
//      Container(
//        margin: EdgeInsets.only(right: 16.0),
//        child: CircleAvatar(child: Text('B')),
//      ),
//      Expanded(
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            Text(this.name, style: TextStyle(fontWeight: FontWeight.bold)),
//            Container(
//              margin: EdgeInsets.only(top: 5.0),
//              child: Text(text),
//            ),
//          ],
//        ),
//      ),
//    ];
//  }

  List<Widget> myMessage(context) {
    return <Widget>[
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Bubble(
              padding: BubbleEdges.all(10),
              alignment: Alignment.topRight,
              nip: BubbleNip.rightTop,
              color: kContainerColor3,
              child: Text(
                text,
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
    ];
  }

//  List<Widget> myMessage(context) {
//    return <Widget>[
//      Expanded(
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.end,
//          children: <Widget>[
//            Text(this.name, style: Theme.of(context).textTheme.subhead),
//            Container(
//              margin: EdgeInsets.only(top: 5.0),
//              child: Text(text),
//            ),
//          ],
//        ),
//      ),
//      Container(
//        margin: EdgeInsets.only(left: 16.0),
//        child: CircleAvatar(
//            child: Text(
//              this.name[0],
//              style: TextStyle(fontWeight: FontWeight.bold),
//            )),
//      ),
//    ];
//  }
}
