import 'package:flutter/material.dart';
import 'package:telenor_hackfair_app/constants.dart';
import 'package:telenor_hackfair_app/screens/chat_screen.dart';
import 'package:telenor_hackfair_app/screens/settings_screen.dart';
import 'package:telenor_hackfair_app/screens/walkthrough_flow.dart';
import 'screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'screens/home_screen.dart';
import 'screens/walkthrough_flow.dart';
import 'screens/walkthrough_flow.dart';
import 'services/user_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await User.setInitialUserPreferences();
  return runApp(HackFairApp());
}

class HackFairApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Telenor HackFair Application Project',
      theme: ThemeData(
        backgroundColor: Color(0xFF1F344B),
        canvasColor: Color(0xFF152A3F),
        primaryColor: Color(0xFFFC5B19),
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: Colors.white,
              displayColor: Colors.white,
            ),
      ),
      initialRoute: WalkthroughFlow.id,
      debugShowCheckedModeBanner: false,
      routes: {
        HomeScreen.id: (context) => HomeScreen(),
        SettingsScreen.id: (context) => SettingsScreen(),
        WalkthroughFlow.id: (context) => WalkthroughFlow(),
        ChatScreen.id: (context) => ChatScreen(),
      },
    );
  }
}
